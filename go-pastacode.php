<?php
/*********************************************
=== Go Pastacode ===

Script Name: Go Pastacode
Script URI: https://bitbucket.org/willybahuaud/go-pastacode
Author URI: http://wabeo.fr
Author: Willy Bahuaud
Version: 0.9
License: GPL
**********************************************/

// Load WordPress
while( !is_file( 'wp-load.php' ) ) {
	if( is_dir( '..' ) ) 
		chdir( '..' );
	else
		die( 'EN: Could not find WordPress! FR : Impossible de trouver WordPress !' );
}
require_once( 'wp-load.php' );

// if an action is triggered
if( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'migrate' ) :

	function exclude_pastacode( $matches ) {
		$balises = 'POUETPOUETPOUET%sAHAHAHAHAHHAHAHAH';
		return sprintf( $balises, rawurlencode( $matches[0] ) );
	}

	function restore_pastacode( $matches ) {
		return rawurldecode( $matches[2] );
	}

	function filtre_arguments( $matchedatt ) {
		$atts = '';
		$langs  = array(
	        'markup',       
	        'css',          
	        'javascript',   
	        'php',          
	        'c',            
	        'c++',          
	        'java',         
	        'sass',         
	        'python',       
	        'sql',          
	        'ruby',         
	        'coffeescript', 
	        'bash',         
	    );
		foreach ( $matchedatt[1] as $k => $attr ) {
			switch ( $attr ) {
				case 'lang':
					// replace wrong languages
					if ( 'html' == $matchedatt[2][ $k ] )
						$matchedatt[2][ $k ] = 'markup';
					if ( 'js' == $matchedatt[2][ $k ] )
						$matchedatt[2][ $k ] = 'javascript';
					if ( in_array( $matchedatt[2][ $k ], $langs ) ) {
						$atts .= ' lang="' . $matchedatt[2][ $k ] . '"';
					}
					break;
				case 'title':
					$atts .= ' message="' . esc_attr( $matchedatt[2][ $k ] ) . '"';
				case 'mark':
					$atts .= ' highlight="' . esc_attr( $matchedatt[2][ $k ] ) . '"';
				default :
			}
		}
		return $atts;
	}

	function replace_pre_tags( $matches ) {
		$pastacode = '[pastacode provider="manual"%s]<pre><code>%s</code></pre>[/pastacode]';
		$code = preg_replace( '/(<\/?(pre|code)(.*)>)/isU', '', $matches[2] );
		// filtre arguments
		if ( preg_match_all( '/\s?(\w*)=[\"\']?([\w\+-]*)[\"\']/', $matches[1], $matchedatt ) ) {
			$attr = filtre_arguments( $matchedatt );
		} else {
			$attr = ' lang="markup"';
		}
		$content = sprintf( $pastacode, $attr, $code );
		return $content;
	}

	function find_pre_tags( $str ) {
		$regex = '/<pre(.*)>(.*)<\/pre>/isU';
		$content = preg_replace_callback( $regex, 'replace_pre_tags', $str );
		return $content;
	}

	$all = new WP_Query( array( 'post_type' => 'any', 'posts_per_page' => -1 ) );
	$s = array();
	if ( $all->have_posts() ) : while ( $all->have_posts() ) : $all->the_post();
		// hide shortcodes
		$content = preg_replace_callback( '/' . get_shortcode_regex() . '/s', 'exclude_pastacode', $all->post->post_content );
		// process replacement
		$content = find_pre_tags( $content );
		// restore shortcodes
		$content = preg_replace_callback( '/(POUETPOUETPOUET)(.*)(AHAHAHAHAHHAHAHAH)/', 'restore_pastacode', $content );
		// save changes
		// $s[] = $content;
		wp_update_post( array( 'ID' => $all->post->ID, 'post_content' => $content ), false );
	endwhile; endif;
	// die( var_dump( $s ) );

endif;

// HTML CODE
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//FR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Go Pastacode 0.9</title>
	</head>
	<body>
	<h1>Migration vers Pastacode</h1>
	<?php if ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'migrate' ) : ?>
		<p><strong>Migration effectuée !</strong></p>
	<?php else : ?>
		<p>Ce script permet de migrer les codes insérés dans vos articles à partir de plugins de coloration syntaxiques (tels que Crayon Highlighter ou WP-Syntax) vers <a href="http://wordpress.org/plugins/pastacode/" title="le plugin de coloration syntaxique Wordpress ultime">Pastacode</a></p>
		<p><strong>Faites une sauvegarde de votre base de donnée avant toute chose !!</strong><br /><br /><a href="?action=migrate">Lancer la migration</a></p>
	<?php endif; ?>
</body>
</html>